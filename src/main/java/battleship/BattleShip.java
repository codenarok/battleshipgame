package battleship;

import java.util.*;

public class BattleShip {

	public ArrayList<String> location;

	private String name;

	public String checkYourself(String userGuess) {

		String result = "Miss!";

		int index = location.indexOf(userGuess);

		if (index >= 0) {
			location.remove(index);

			if (location.isEmpty()) {
				result = "Kill";
			} else {
				result = "Hit!";
			}
		}

		return result;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public void setName(String name) {
		this.name = name;
	}
}
