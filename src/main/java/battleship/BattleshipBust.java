package battleship;

import java.util.ArrayList;

public class BattleshipBust {
	private ArrayList<BattleShip> battleshipList = new ArrayList<BattleShip>();
	private GameHelper helper = new GameHelper();
	private int numOfGuesses = 0;

	public void setUpGame() {

		BattleShip one = new BattleShip();
		one.setName("small");
		BattleShip two = new BattleShip();
		two.setName("medium");
		BattleShip three = new BattleShip();
		three.setName("big");

		battleshipList.add(one);
		battleshipList.add(two);
		battleshipList.add(three);

		System.out.println("Your goal is to sink three ships.");
		System.out.println("small, medium and big ships.");
		System.out.println("Try to sink them all with fewer guesses");

		// Created this instance to update available locations 
		LocationChanger lc = new LocationChanger();
		// this arraylist stores the 15 possible location
		ArrayList<String> pbLoc = new ArrayList<String>();
		
		for (int i = 0; i <= 20; i++) {
			// storing the 15 possible locations inside the arraylist
			pbLoc.add(String.valueOf(i));
		}
		
		//setting the possbile locations into the instance variable
		lc.setPossibleLocations(pbLoc);
		
		for (BattleShip shipSet : battleshipList) {
			//we are putting our lastest possible locations as an argument into the place ship method so it generates random places depending on the available spots
			ArrayList<String> newLocation = helper.placeTheShip(3,lc.getPossibleLocations());
			
			for(int i =0 ; i<3; i++) {
				//this removes the used spots from our possible location instance variable so next time we can have the unused spots.
			lc.getPossibleLocations().remove(lc.getPossibleLocations().indexOf(String.valueOf(newLocation.get(i))));
			}
			//System.out.print(lc.getPossibleLocations()); //test
			shipSet.setLocation(newLocation);
		}
	}

	public void startPlaying() {
		while (!battleshipList.isEmpty()) {
			String userGuess = helper.getUserInput("Enter a guess");
			checkUserGuess(userGuess);
		}

		finishGame();
	}

	public void checkUserGuess(String userGuess) {

		numOfGuesses++;
		String result = "miss";
		for (BattleShip battleshipTest : battleshipList) {
			result = battleshipTest.checkYourself(userGuess);
			if (result.equals("Hit!")) {
				break;
			}
			if (result.equals("Kill")) {
				battleshipList.remove(battleshipTest);
				break;
			}
		}

		System.out.println(result);

	}

	public void finishGame() {
		System.out.println("All ships are dead!");
		if (numOfGuesses <= 18) {
			System.out.println("it took you " + numOfGuesses + " guesses.");
		} else {
			System.out.println("Took you long enough. " + numOfGuesses + " guesses.");
			System.out.println("Fish are dancing with your options.");
		}

	}

}
