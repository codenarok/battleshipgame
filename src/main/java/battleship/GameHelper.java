package battleship;

import java.util.ArrayList;

import java.util.Scanner;

public class GameHelper {

	//

	public ArrayList<String> placeTheShip(int size, ArrayList<String> possibleLocations) {

		ArrayList<String> locations = new ArrayList<String>();
		// while tanımlanacak active true iken
		// int neededIndex = possibleLocations.indexOf(randNum);
		boolean isActive = true;
		while (isActive == true) {

			int randNum = (int) (Math.random() * (possibleLocations.size() - 4));
			// we are testing whether the 3 locations are sequential numbers
			if (Integer.parseInt((possibleLocations.get(randNum + 2)))
					- Integer.parseInt(possibleLocations.get(randNum)) == 2) {

				for (int i = 0; i < size; i++) {
					// here we are getting our possible location as a parameter from BattleshipBust
					// and using it to pick the available spots for our ship.

					locations.add(possibleLocations.get(randNum + i));
					isActive = false;
				}
			}

		}

		BattleShip game = new BattleShip();
		game.setLocation(locations);
		return locations;
	}

	public String getUserInput(String string) {
		Scanner computerRequest = new Scanner(System.in); // Create a Scanner object
		System.out.println(string);
		String userInput = computerRequest.nextLine(); // Read user input

		return userInput;
	}

}
