package battleship;

import java.util.ArrayList;

public class LocationChanger {

	public ArrayList<String> possibleLocations;

	public ArrayList<String> getPossibleLocations() {
		return possibleLocations;
	}

	public void setPossibleLocations(ArrayList<String> possibleLocations) {
		this.possibleLocations = possibleLocations;
	}
}
