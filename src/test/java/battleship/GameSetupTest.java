package battleship;

import static org.junit.Assert.*;

import java.util.ArrayList;


import org.junit.Test;



public class GameSetupTest {

	@Test
	public void testGameSetup() {
		BattleShip gs = new BattleShip();
		ArrayList<String> location = new ArrayList<String>();
		location.add("2");
		location.add("3");
		location.add("4");
		gs.setLocation(location);
		String userGuess = "2";
		assertEquals("Hit!", gs.checkYourself(userGuess));
	}
	
	
}
